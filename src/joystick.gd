extends Control

# The default meaning of "local" in this file is "local to TextureRect".
# XXX radius is not the radius of TextureRect but the radius up to which
# movement in unrestricted.

var button_size
var joystick_center
var radius
var active = false

func _ready():
  button_size = $TextureRect/TextureButton.normal.get_size()
  joystick_center = $TextureRect.rect_position + ($TextureRect.rect_size)/2
  radius = $TextureRect.rect_size.x/2 - button_size.x/2
  $TextureRect/TextureButton.position = joystick_center - button_size/2

func _process(delta):
  if (active):
    var local_mouse_displacement = get_local_mouse_position() - joystick_center
    var local_mouse_distance = sqrt(pow(local_mouse_displacement.x, 2) + pow(local_mouse_displacement.y, 2))
    var local_joystick_displacement = joystick_center - button_size/2

    # if the cursor is too far away then clamp to edges, otherwise move freely
    if (local_mouse_distance > radius):
      $TextureRect/TextureButton.position = local_joystick_displacement + local_mouse_displacement/local_mouse_distance*radius
    else:
      $TextureRect/TextureButton.position = local_joystick_displacement + local_mouse_displacement

func get_keyboard_input():
  var movement = Vector2(0, 0)
  if (Input.is_action_pressed("ui_up")):
    movement += Vector2(0, -1)
  if (Input.is_action_pressed("ui_down")):
    movement += Vector2(0, 1)
  if (Input.is_action_pressed("ui_left")):
    movement += Vector2(-1, 0)
  if (Input.is_action_pressed("ui_right")):
    movement += Vector2(1, 0)
  return movement

func get_input_vector():
  var input
  if (active):
    input = ($TextureRect/TextureButton.position - joystick_center + button_size/2)/radius
  else:
    input = get_keyboard_input().normalized()

  return input*input*input;

func get_angle():
  var local_mouse_displacement = get_local_mouse_position() - joystick_center
  return atan2(local_mouse_displacement.y, local_mouse_displacement.x)

func get_coefficient():
  var relative_joystick_position = $TextureRect/TextureButton.position  - joystick_center + button_size/2
  return pow(sqrt(pow(relative_joystick_position.x, 2) + pow(relative_joystick_position.y, 2))/radius, 2)

# Return to center also managed here
func _on_TextureButton_released():
  active = false
  $TextureRect/TextureButton.position = joystick_center - button_size/2

func _on_TextureButton_pressed():
  active = true
